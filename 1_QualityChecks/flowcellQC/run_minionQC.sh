#!/bin/bash
#SBATCH --job-name=R_QC_combined4genome
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=150G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load R/4.0.3

#make combined file from all flow cells and output graphs as pdfs
#Rscript MinIONQC.R -c TRUE -f 'pdf' -i /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/sequencing_summaryFiles/ -o all_flowcells_combined_QC -p 8

#Rscript MinIONQC.R -c TRUE -f 'png' -i /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/sequencing_summaryFiles/ -o all_flowcells_combined_QC_png -p 8

#combined run of just the runs used for the genome assembly (-WGA runs)
#Rscript MinIONQC.R -c TRUE -f 'pdf' \
#-i /projects/EBP/Oneill/reads/nanopore/promethion/salp_promethion/salp_summary_files_4GenomeAssemb/ -o flowcells_4GenomeAssemb_combined_QC -p 8

#Get stats for each run
Rscript MinIONQC.R -f 'png' -i /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/sequencing_summaryFiles/ -o individual_flowcells_combined_QC_png -p 8
