#!/bin/bash
#SBATCH --job-name=Nanoplot_Saspera_Oct032019
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load NanoPlot/1.21.0

#Oct 3, 2019 LSK109 library of S. aspera (unsheared)
NanoPlot --summary /labs/Oneill/reads/nanopore/minion/salp_aspera_minion/2019OCT30_Sasp-AR06-18_MIN106_FAL53831_LSK109/2019OCT30_Sasp-AR06-18_MIN106_FAL53831_LSK109/20191030_1807_MN17898_FAL53831_99a6adf8/fastq/sequencing_summary.txt \
   -o 2019Otc03_Saspera_LSK109_ABD436_Nanoplot \
   -p 2019Oct03_Saspera_LSK109_ABD436_Nanoplot \
   -t 4 \
   --verbose

#June 2021 run A
NanoPlot --summary /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_A_PAG25939/20210601_1834_3D_PAG25939_85be1ccc/sequencing_summary_PAG25939_aafefd32.txt \
       -o 2021June1_sAsp_LSK109_A_Nanoplot \
       -p 2021June1_sAsp_LSK109_A_Nanoplot \
       -t 4 \
       --verbose

#June 2021 run B
NanoPlot --summary /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_B_PAG25945/20210601_1834_3F_PAG25945_b6df17ac/sequencing_summary_PAG25945_921b6157.txt \
       -o 2021June1_sAsp_LSK109_B_Nanoplot \
       -p 2021June1_sAsp_LSK109_B_Nanoplot \
       -t 4 \
       --verbose
