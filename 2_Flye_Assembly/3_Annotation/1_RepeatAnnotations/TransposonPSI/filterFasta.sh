#!/bin/bash
#SBATCH --job-name=filter_TransposonPSI
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=30G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

#remove all contigs <3kb
awk '!/^>/ { next } { getline seq } length(seq) >= 50 { print $0 "\n" seq }' /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/TransposonPSI/Saspera_AR06-14_TransposonPSI_bestPerLocus.fasta > Saspera_AR06-14_TransposonPSI_bestPerLocus_filter50bp.fasta
