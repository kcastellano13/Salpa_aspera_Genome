#!/bin/bash
#SBATCH --job-name=bedtools_getfasta
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load BEDtools/2.29.0

#-s force strandedness (reverse complements seqs)
#-name Use the “name” column in the BED file for the FASTA headers in the output FASTA file - otherwise it is cut to contig and region

bedtools getfasta -fi /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta \
-bed /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/TransposonPSI/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta.TPSI.allHits.chains.bestPerLocus.gff3 \
-s -fullHeader -fo Saspera_AR06-14_TransposonPSI_bestPerLocus.fasta
