#!/bin/bash
#SBATCH --job-name=RepeatModel_Saspera
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 20
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=200G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load RepeatModeler/2.01

RepeatModeler -database /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatModeler/SasperaAR0613 -pa 20
