#!/bin/bash
#SBATCH --job-name=RepeatClassifier_2019salpmedaka_combinedlib
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


/isg/shared/apps/RepeatModeler/1.0.8/RepeatClassifier \
-consensi /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/combined_clusteredLibrary/Saspera_combinedRepeatLib_cluster0.8.fa
