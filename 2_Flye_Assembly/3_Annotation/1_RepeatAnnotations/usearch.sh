#!/bin/bash
#SBATCH --job-name=usearch_repeat_saspera
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=50G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load usearch/9.0.2132

##unclassified for Salp 2019 Medaka genome
usearch -cluster_fast Saspera_combinedRepeatLib.fa -id 0.8 -centroids Saspera_combinedRepeatLib_cluster0.8.fa
