#!/bin/bash
#SBATCH --job-name=RMonly_saspera_repeatmodelOnly
#SBATCH --qos=general
#SBATCH --partition=general
#SBATCH --mem-per-cpu 15G
#SBATCH --nodes 1
#SBATCH --cpus-per-task 10
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH --mail-type=END

echo -n > saspera_denovolib.out
scontrol show job $SLURM_JOB_ID
module load RepeatMasker/4.0.9-p2

genome=SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta
use_ram_only=true
if $use_ram_only ; then
    tmpfs=/dev/shm/$USER
    mkdir -p $tmpfs
    pushd $tmpfs
fi
RepeatMasker \
    -lib /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatModeler/SasperaAR0613-families.fa \
    -xsmall \
    -no_is \
    -gff \
    -parallel $SLURM_CPUS_PER_TASK \
    $OLDPWD/$genome
if $use_ram_only ; then
    popd
        # This directory should normally empty;
        # but it will have files if there were errors.
    output=repeatmasker_output
    rm -rf $output
    mv -v $tmpfs $output
fi
