#!/bin/bash
#SBATCH --job-name=LTRharvest
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mem=250G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load genometools/1.5.10

gt ltrharvest -index Saspera_AR0613_INDEX -gff3 Saspera_AR0613_LTRharvest.gff3 -out Saspera_AR0613_LTRharvest.fasta
