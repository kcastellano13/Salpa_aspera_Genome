#!/bin/bash
#SBATCH --job-name=LTRdigest_Saspera
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mem=250G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load genometools/1.5.10
module load hmmer/3.2.1

gt -j 5 ltrdigest -outfileprefix Saspera_AR0613_LTRharvestDigest \
-hmms /projects/EBP/Oneill/salp_genome_assembly/pfam_GyDB_HMM/*.hmm \
-matchdescstart /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/LTRharvest/Saspera_AR0613_LTRharvestSORTED.gff3 \
Saspera_AR0613_INDEX > Saspera_AR0613_LTRharvestDigest.gff3
