#!/bin/bash
#SBATCH --job-name=indexLTRharvest
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=50G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

#module load genometools/1.6.1
module load genometools/1.5.10

gt suffixerator -db /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta \
-indexname Saspera_AR0613_INDEX -suf -lcp -des -ssp -sds -dna -lossless -v
