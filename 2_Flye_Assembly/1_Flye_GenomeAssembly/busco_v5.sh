#!/bin/bash
#SBATCH --job-name=SaspFLYE_buscov5
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=32G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err

module load busco/5.0.0

#FLYE assembly S aspera - AR06-13 - only long read data
busco -i /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/Saspera_flye_June2021/assembly.fasta \
-o BUSCO_v5_Saspera_AR06-13_Flye \
-l metazoa_odb10 \
-m geno -c 32

#FLYE assembly S aspera - AR06-13 - only long read data - 3kb limit
busco -i /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/3kblim/assembly_3kblim.fasta \
-o BUSCO_v5_Saspera_AR06-13_Flye3kblim \
-l metazoa_odb10 \
-m geno -c 32


#FLYE assembly S aspera - AR06-13 - only long read data - 3kb limit - medaka polish
busco -i /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/medaka/medaka_flye_SasperaAR06-13/consensus.fasta \
-o BUSCO_v5_Saspera_AR06-13_Flye3kblim_medaka \
-l metazoa_odb10 \
-m geno -c 32

#FLYE assembly S aspera - AR06-13 - only long read data - 3kb limit - medaka polish - purge dups
busco -i /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus/seqs/consensus.purged.fa \
-o BUSCO_v5_Saspera_AR06-13_Flye3kblim_medaka_purgeDups \
-l metazoa_odb10 \
-m geno -c 32

#FLYE assembly S aspera - AR06-13 - only long read data - 3kb limit - medaka polish - purge dups - contam removed
busco -i /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta \
-o BUSCO_v5_Saspera_AR06-13_Flye3kblim_medaka_purgeDups_contamRemoved \
-l metazoa_odb10 \
-m geno -c 20
