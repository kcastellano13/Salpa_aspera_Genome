#!/bin/bash
#SBATCH --job-name=quast_saspera_flye3kblim
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load quast/5.0.2

#s aspera AR06-13 - only the 1D genomic DNA by ligation runs - flye assembled
quast.py /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/Saspera_flye_June2021/assembly.fasta \
-o sasperaAR0613_Flye -t 10

#s aspera AR06-13 - only the 1D genomic DNA by ligation runs - flye assembled - 3kb limit
quast.py /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/3kblim/assembly_3kblim.fasta \
-o sasperaAR0613_Flye3kblim -t 10

#s aspera AR06-13 - only the 1D genomic DNA by ligation runs - flye assembled - 3kb limit - medaka polish
quast.py /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/medaka/medaka_flye_SasperaAR06-13/consensus.fasta \
-o sasperaAR0613_Flye3kblim_medaka -t 10

#s aspera AR06-13 - only the 1D genomic DNA by ligation runs - flye assembled - 3kb limit - medaka polish - purge dups
quast.py /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus/seqs/consensus.purged.fa \
-o sasperaAR0613_Flye3kblim_medaka_purgeDups -t 10

#s aspera AR06-13 - only the 1D genomic DNA by ligation runs - flye assembled - 3kb limit - medaka polish - purge dups - contam removed
quast.py /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta \
-o sasperaAR0613_Flye3kblim_medaka_purgeDups_contamRemoved -t 10

#s aspera AR06-13 - only the 1D genomic DNA by ligation runs - flye assembled - 3kb limit - medaka polish - purge dups with manual cutoffs
quast.py /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus/manual_purgeDUP/purged.fa \
-o sasperaAR0613_Flye3kblim_medaka_purgeDupsManualCutoffs -t 10
