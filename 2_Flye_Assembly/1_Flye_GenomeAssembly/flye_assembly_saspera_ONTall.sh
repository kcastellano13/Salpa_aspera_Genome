#!/bin/bash
#SBATCH --job-name=flye_saspera
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=380G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load flye/2.4.2

#Saspera assembly - combined Oct 2019 and June 2021 1d genomic ligation runs (no WGA)
flye --nano-raw /archive/projects/EBP/roneill/Salp/saspera_genomeassemb/FINALassembly_2021/Saspera_AR06-13_allCombined.fq \
        --genome-size 600m \
        --out-dir Saspera_flye_June2021 \
        --threads 8