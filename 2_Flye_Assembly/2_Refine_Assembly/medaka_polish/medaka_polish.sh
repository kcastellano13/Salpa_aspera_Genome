#!/bin/bash
#SBATCH --job-name=medaka_flye_Sasp
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mem=500G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load medaka/1.3.2
module load python/3.6.3
module unload tabix/0.2.6
module load zlib/1.2.11

cp /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/3kblim/assembly_3kblim.fasta /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/medaka/

medaka_consensus -i /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/Saspera_AR06-13_allCombined.fq \
-d assembly_3kblim.fasta -o medaka_flye_SasperaAR06-13 -t 32 -m r941_prom_high_g4011