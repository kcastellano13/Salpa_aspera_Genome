#!/bin/bash
#SBATCH --job-name=cent_copepods_sasperagenom50min
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=125G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load centrifuge/1.0.4-beta

##S. thompsoni polished shasta assembly - look for antarctic copepod contamination
#centrifuge -f -x /projects/EBP/Oneill/salp_genome_assembly/Salp451_2019Sep_genomeassemb/centrifuge/centrifuge_phytozoo/myDB/library/copepod/antarctic_copepods \
#       --report-file cent_salp_copepods_minhit100.tsv --quiet --min-hitlen 100 \
#       /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta


centrifuge -f -x /projects/EBP/Oneill/salp_genome_assembly/Salp451_2019Sep_genomeassemb/centrifuge/centrifuge_phytozoo/myDB/library/copepod/antarctic_copepods \
        --report-file cent_salp_copepods_minhit50.tsv --quiet --min-hitlen 50 \
        /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta