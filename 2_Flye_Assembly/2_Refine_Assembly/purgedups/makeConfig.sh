#!/bin/bash
#SBATCH --job-name=purgedups_makeConfig
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load purge_dups/1.0.0

pd_config.py -l /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups \
-n config_Saspera_flye_3kblim_medaka.json \
/projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/medaka/medaka_flye_SasperaAR06-13/consensus.fasta pb.fofn