#!/bin/bash
#SBATCH --job-name=shasta_saspera_all
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 17
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=450G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load shasta/0.7.0

#all promethion 1D genomic DNA by ligation (no WGA)
shasta --input /archive/projects/EBP/roneill/Salp/saspera_genomeassemb/FINALassembly_2021/Saspera_AR06-13_allCombined.fasta \
--Reads.minReadLength 500 --memoryMode anonymous --memoryBacking 4K --threads 17 --Align.minAlignedMarkerCount 50