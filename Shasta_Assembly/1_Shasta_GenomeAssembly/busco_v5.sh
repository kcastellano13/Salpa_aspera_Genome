#!/bin/bash
#SBATCH --job-name=Sasp_buscov5
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=32G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err

module load busco/5.0.0

#Shasta assembly S aspera - AR06-13 - only long read data
busco -i /archive/projects/EBP/roneill/Salp/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/ShastaRun/Assembly.fasta \
-o BUSCO_v5_Saspera_AR06-13_shasta \
-l metazoa_odb10 \
-m geno -c 32

#Shasta assembly S aspera - AR06-13 - only long read data + Medaka polished
busco -i /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/medaka/medaka_shasta_SasperaAR06-13/consensus.fasta \
-o BUSCO_v5_Saspera_AR06-13_shasta_Medaka \
-l metazoa_odb10 \
-m geno -c 32

#Shasta assembly S aspera - AR06-13 - only long read data + Medaka polished + 3kb lim
busco -i /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/medaka/medaka_shasta_SasperaAR06-13/consensus_3kblim.fasta \
-o BUSCO_v5_Saspera_AR06-13_shasta_Medaka_3kblim \
-l metazoa_odb10 \
-m geno -c 32

#Shasta assembly S aspera - AR06-13 - only long read data + Medaka polished + 3kb lim + haplotigs purged (purge dups)
busco -i /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/purge_dups/consensus_3kblim/seqs/consensus_3kblim.purged.fa \
-o BUSCO_v5_Saspera_AR06-13_shasta_Medaka_3kblim_purgeDups \
-l metazoa_odb10 \
-m geno -c 32

#Shasta assembly S aspera - AR06-13 - only long read data + Medaka polished + 3kb lim + haplotigs purged (purge dups) + contamination removed
busco -i /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/centrifuge/SasperaAR06-13_shasta_medaka_3kblim_purgeDup_ContamRemoved.fasta \
-o BUSCO_v5_Saspera_AR06-13_shasta_Medaka_3kblim_purgeDups_contamRemoved \
-l metazoa_odb10 \
-m geno -c 32
