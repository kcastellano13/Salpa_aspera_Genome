#!/bin/bash
#SBATCH --job-name=quast_saspera_shasta_medaka_purgeDups_contam
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load quast/5.0.2

#s aspera AR06-13 - only the 1D genomic DNA by ligation runs - Shasta assembled
quast.py /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/ShastaRun/Assembly.fasta \
-o sasperaAR0613_shasta -t 10

##s aspera AR06-13 - only the 1D genomic DNA by ligation runs - Shasta assembled - medaka polished
quast.py /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/medaka/medaka_shasta_SasperaAR06-13/consensus.fasta \
-o sasperaAR0613_shasta_medaka -t 10


##s aspera AR06-13 - only the 1D genomic DNA by ligation runs - Shasta assembled - medaka polished - 3kblim
quast.py /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/medaka/medaka_shasta_SasperaAR06-13/consensus_3kblim.fasta \
-o sasperaAR0613_shasta_medaka_3kblim -t 10

##s aspera AR06-13 - only the 1D genomic DNA by ligation runs - Shasta assembled - medaka polished - 3kblim - haplotigs purged (purge dups)
quast.py /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/purge_dups/consensus_3kblim/seqs/consensus_3kblim.purged.fa \
-o sasperaAR0613_shasta_medaka_3kblim_purgeDups -t 10

##s aspera AR06-13 - only the 1D genomic DNA by ligation runs - Shasta assembled - medaka polished - 3kblim - haplotigs purged (purge dups) - contamination removed
quast.py /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/centrifuge/SasperaAR06-13_shasta_medaka_3kblim_purgeDup_ContamRemoved.fasta \
-o sasperaAR0613_shasta_medaka_3kblim_purgeDups_contamRemoved -t 10
