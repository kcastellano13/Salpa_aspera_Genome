#!/bin/bash
#SBATCH --job-name=centrifuge50min_Salp2019medaka
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load centrifuge/1.0.4-beta

#-x = index base name
#-t Print the wall-clock time required to load the index files and align the reads. This is printed to the "standard error" ("stderr") filehandle. Default: off.
#-p threads
#IMPORTANT - run each command separate because it outputs info into the .out file you will need in order to remove contaminated contigs

##Shasta assembly Sept 2019 - 3kb lim - min read hit = 100bp
centrifuge -f -x /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v  --report-file centrifuge_salp_shasta2019medaka_report_minhit100.tsv \
--quiet --min-hitlen 100 /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/purge_dups/consensus_3kblim/seqs/consensus_3kblim.purged.fa

##Shasta assembly Sept 2019 - 3kb lim - min read hit = 50bp
centrifuge -f -x /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v --report-file centrifuge_salp_shasta2019medaka_report_minhit50.tsv \
--quiet --min-hitlen 50 /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/purge_dups/consensus_3kblim/seqs/consensus_3kblim.purged.fa
