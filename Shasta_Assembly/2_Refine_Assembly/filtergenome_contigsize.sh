#!/bin/bash
#SBATCH --job-name=3kblim_shastaMedaka
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

#remove all contigs <3kb
awk '!/^>/ { next } { getline seq } length(seq) >= 3000 { print $0 "\n" seq }' /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/medaka/medaka_shasta_SasperaAR06-13/consensus.fasta > consensus_3kblim.fasta