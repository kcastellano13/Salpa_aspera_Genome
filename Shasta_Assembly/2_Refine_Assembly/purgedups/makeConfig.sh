#!/bin/bash
#SBATCH --job-name=purgedups_makeConfig
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load purge_dups/1.0.0

pd_config.py -l /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/purge_dups \
-n config_Saspera_shasta_medaka_3kblim.json \
/projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/medaka/medaka_shasta_SasperaAR06-13/consensus_3kblim.fasta pb.fofn