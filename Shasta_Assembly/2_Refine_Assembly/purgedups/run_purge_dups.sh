#!/bin/bash
#SBATCH --job-name=purgedup_saspera_shasta
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=200G
#SBATCH --mail-user=kate.castellano@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load minimap2/2.18
module load python/3.8.1
module load purge_dups/1.0.0

run_purge_dups.py -p bash config_Saspera_shasta_medaka_3kblim.json /isg/shared/apps/purge_dups/1.0.0/src/ Saspera_AR06-13_shasta_medaka_3kblim