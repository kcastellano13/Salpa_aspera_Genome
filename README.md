<h2><em>Salpa aspera</em> genome sequencing and assembly </h2>
check

Sample: AR06-18 
HMW extraction Lab notebook #2 pg 99-103
ONT Library prep (Genomic DNA by ligation) pg 104 and printed documents (Nicole did for me so see her notebook for details)
    One minion run 
    _Kit:_ LSK109
    _Flowcell:_ MIN106_FAL5381

ONT Library prep (Genomic DNA by ligation) done by Nicole Pauloski in May 2021 and ran two flowcells on the promethion (PR002)
     _kit:_ LSK109
     - size selcted to 20 kb with covaris g-tube

A. Minion run - genomic DNA by ligation - October 30, 2019<br>
_raw reads:_ /projects/EBP/Oneill/reads/nanopore/minion/salp_aspera_minion/2019OCT30_Sasp-AR06-18_MIN106_FAL53831_LSK109/

B. Promethion runs - genomic DNA by ligation - June 2021
_raw reads:_ 
/projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_A_PAG25939
/projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_B_PAG25945

C. Flongle run - WGA - December 2019-Jan 2020<br>
/projects/EBP/Oneill/reads/nanopore/flongle/2020Jan02_Saspera_WGA_ABN950_LSK109/<br>

D. Promethion run - WGA - January 2020<br>
    /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2020Jan_SasperaWGA_PRO002_LSK109/<br>

E. Promethion run - WGA - March 2020 (2 flow cells)<br>
    /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2020MAR17_sAsp_WGA_LSK109/2020MAR17_sAsp_WGA_LSK109_1_PAE63720/<br>
    /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2020MAR17_sAsp_WGA_LSK109/2020MAR17_sAsp_WGA_LSK109_2_PAE63720/<br>
    *see step 8 for details with this set of data*
 

**1.Nanoplot**
     A.Minion files done previously
          _Input:_ summary file
          /projects/EBP/Oneill/reads/nanopore/minion/salp_aspera_minion/2019OCT30_Sasp-AR06-18_MIN106_FAL53831_LSK109/2019OCT30_Sasp-AR06-18_MIN106_FAL53831_LSK109/20191030_1807_MN17898_FAL53831_99a6adf8/fastq/sequencing_summary.txt

          _Output:_ folder with stats and images named: 2019Otc03_Saspera_LSK109_ABD436_Nanoplot/
          /projects/EBP/Oneill/reads/nanopore/minion/salp_aspera_minion/2019OCT30_Sasp-AR06-18_MIN106_FAL53831_LSK109/2019OCT30_Sasp-AR06-18_MIN106_FAL53831_LSK109/20191030_1807_MN17898_FAL53831_99a6adf8/2019Otc03_Saspera_LSK109_ABD436_Nanoplot/

          _Nanoplot script and location:_
          /projects/EBP/Oneill/reads/nanopore/minion/salp_aspera_minion/2019OCT30_Sasp-AR06-18_MIN106_FAL53831_LSK109/2019OCT30_Sasp-AR06-18_MIN106_FAL53831_LSK109/20191030_1807_MN17898_FAL53831_99a6adf8/Nanoplot.sh

     B. Promethion Runs
          _input:_
          RunA: /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_A_PAG25939/20210601_1834_3D_PAG25939_85be1ccc/sequencing_summary_PAG25939_aafefd32.txt
          RunB: /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_B_PAG25945/20210601_1834_3F_PAG25945_b6df17ac/final_summary_PAG25945_921b6157.txt

          _output:_
          RunA:/projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_A_PAG25939/20210601_1834_3D_PAG25939_85be1ccc/Nanoplot/2021June1_sAsp_LSK109_A_Nanoplot
          RunB:/projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_B_PAG25945/20210601_1834_3F_PAG25945_b6df17ac/Nanoplot/2021June1_sAsp_LSK109_B_Nanoplot/

          _script:_
          RunA: /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_A_PAG25939/20210601_1834_3D_PAG25939_85be1ccc/Nanoplot/nanoplot.sh
          RunB: /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_B_PAG25945/20210601_1834_3F_PAG25945_b6df17ac/Nanoplot/nanoplot.sh

**2. Assemble only 1D genomic ligation runs with Shasta (A + B only)**
     A. combine passed fastq reads per run
          cat *.fastq > 2019Oct30_AR06-13_MN17898_combined.fq
               /projects/EBP/Oneill/reads/nanopore/minion/salp_aspera_minion/2019OCT30_Sasp-AR06-18_MIN106_FAL53831_LSK109/2019OCT30_Sasp-AR06-18_MIN106_FAL53831_LSK109/20191030_1807_MN17898_FAL53831_99a6adf8/fastq_NEW/pass

          cat *.fastq > 2021June01_AR06-13_A_PAG25939_combined.fq
               /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_A_PAG25939/20210601_1834_3D_PAG25939_85be1ccc/fastq_pass

          cat *.fastq > 2021June01_AR06-13_B_PAG25945_combined.fq
               /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_B_PAG25945/20210601_1834_3F_PAG25945_b6df17ac/fastq_pass

     B. Combine all 3 runs
          _location:_ /archive/projects/EBP/roneill/Salp/saspera_genomeassemb/FINALassembly_2021

          cp /projects/EBP/Oneill/reads/nanopore/minion/salp_aspera_minion/2019OCT30_Sasp-AR06-18_MIN106_FAL53831_LSK109/2019OCT30_Sasp-AR06-18_MIN106_FAL53831_LSK109/20191030_1807_MN17898_FAL53831_99a6adf8/fastq_NEW/pass/2019Oct30_AR06-13_MN17898_combined.fq Saspera_AR06-13_allCombined.fq
          
          cat /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_A_PAG25939/20210601_1834_3D_PAG25939_85be1ccc/fastq_pass/2021June01_AR06-13_A_PAG25939_combined.fq >> Saspera_AR06-13_allCombined.fq

          cat /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_B_PAG25945/20210601_1834_3F_PAG25945_b6df17ac/fastq_pass/2021June01_AR06-13_B_PAG25945_combined.fq >> Saspera_AR06-13_allCombined.fq

     C. Convert from fastq to fasta using python script
          *python/3.6.3*
          ```
          python fastq2fasta.py Saspera_AR06-13_allCombined.fq Saspera_AR06-13_allCombined.fasta
          ```
          _sript and location_:
          /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/fastq2fasta.py

     D. Assemble using shasta
          1. assemble
               _input:_ Saspera_AR06-13_allCombined.fasta (Oct 2019 minion and 2 promethion runs from June 2021)
              /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/

               _output:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/shasta.sh
          2. check quality with quast
               _input:_

               _output:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/QUAST

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/QUAST/quast.sh

          3. check quality with busco
               _input:_shasta genome
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/ShastaRun/Assembly.fasta

               _output:_ folder with stats
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/BUSCO/sasperaAR0613_shasta/

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/BUSCO/busco_v5.sh


     E. polish with medaka
          1. polish
               _input:_ shasta assembly and fastq reads
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/ShastaRun/Assembly.fasta
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/Saspera_AR06-13_allCombined.fq

               _output:_ polished genome
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/medaka/medaka_shasta_SasperaAR06-13/consensus.fasta

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/medaka/medaka_polish.sh

          2. check quality with quast
               _input:_ medaka polish genome
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/medaka/medaka_shasta_SasperaAR06-13/consensus.fasta

               _output:_ folder with stats
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/QUAST/sasperaAR0613_shasta_medaka

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/QUAST/quast.sh


          3. check quality with busco
               _input:_ medaka polished genome
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/medaka/medaka_shasta_SasperaAR06-13/consensus.fasta

               _output:_ Saspera_AR06-13_shasta_medaka_minimap.sam
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/BUSCO

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/BUSCO/busco_v5.sh
     
     F. Impose 3kb limit (normally do before but forgot)
          _input:_ consensus.fasta
          /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/medaka/medaka_shasta_SasperaAR06-13

          _output:_ consensus_3kblim.fasta
          /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/medaka/medaka_shasta_SasperaAR06-13

          _script:_
          /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/medaka/medaka_shasta_SasperaAR06-13/filtergenome_contigsize.sh

     G. Purge haplotigs
          1. map reads to genome using minimap2
               _input:_ polished genome and combined fastq reads
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/medaka/medaka_shasta_SasperaAR06-13/consensus_3kblim.fasta 
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/Saspera_AR06-13_allCombined.fq 

               _output:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/minimap2/

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/minimap2/minimap2.sh
          
          2. convert sam to bam
               _input:_ Saspera_AR06-13_shasta_medaka_minimap.sam
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/minimap2/

               _output:_ Saspera_AR06-13_shasta_medaka_minimap.bame and Saspera_AR06-13_shasta_medaka_minimap.sorted.bam
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/minimap2/

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/minimap2/samtools_cvrtsortindex.sh
          
          3. calculate mapping stats
               _input:_ Saspera_AR06-13_shasta_medaka_minimap.sam
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/minimap2/

               _output:_ Saspera_AR06-13_shasta_medaka_minimap_summary.tsv
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/minimap2/

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/minimap2/bamsort_stats.sh

               ```
               34656182 + 0 in total (QC-passed reads + QC-failed reads)
               14763867 + 0 secondary
               10576076 + 0 supplementary
               0 + 0 duplicates
               34549465 + 0 mapped (99.69% : N/A)
               0 + 0 paired in sequencing
               0 + 0 read1
               0 + 0 read2
               0 + 0 properly paired (N/A : N/A)
               0 + 0 with itself and mate mapped
               0 + 0 singletons (N/A : N/A)
               0 + 0 with mate mapped to a different chr
               0 + 0 with mate mapped to a different chr (mapQ>=5)
               ```

          *getting errors with purge haplotigs so try purge dups instead*

     H. Purge dups
          1. make config file
               _input:_
               1. pb.fofn which contains path to fq file: /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/Saspera_AR06-13_allCombined.fq
               2. genome location: /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/medaka/medaka_shasta_SasperaAR06-13/consensus_3kblim.fasta

               _output:_ config_Saspera_shasta_medaka_3kblim.json
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/purge_dups/

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/purge_dups/makeConfig.sh
               
          2. Run purge dups
               _input:_ config_Saspera_shasta_medaka_3kblim.json
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/purge_dups/

               _output:_ final purged assembly: consensus_3kblim.purged.fa 
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/purge_dups/consensus_3kblim/seqs
                    *outputs four folders: coverage, purge_dups, seqs and split_aln*

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/purge_dups/run_purge_dups.sh
               
          3. Check stats with quast
               _input:_ shasta assembled, medaka polishied, 3kb lim and purge dup assembly: consensus_3kblim.purged.fa 
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/purge_dups/consensus_3kblim/seqs

               _output:_ sasperaAR0613_shasta_medaka_3kblim_purgeDups folder
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/QUAST/

               _scripts:_
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/QUAST/quast.sh

          4. Check completeness with busco
               _input:_ shasta assembled, medaka polishied, 3kb lim and purge dup assembly: consensus_3kblim.purged.fa 
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/purge_dups/consensus_3kblim/seqs

               _output:_ BUSCO_v5_Saspera_AR06-13_shasta_Medaka_3kblim_purgeDups folder
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/BUSCO/

               _scripts:_
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/BUSCO/busco_v5.sh
        
     I. Centrifuge - to remove contamination
          1. run centrifuge
               _input:_ polished, haplotigs and <3kb contigs purged and the archael, viral, bacterial and human database (isg made)
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/purge_dups/consensus_3kblim/seqs/consensus_3kblim.purged.fa
               /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v

               _output:_ centrifuge_salp_shasta2019medaka_report_minhit100.tsv, centrifuge_salp_shasta2019medaka_report_minhit50.tsv with summary of hits and centrifuge100min_Salp2019medaka_3169700.out, centrifuge50min_Salp2019medaka_3169700.out with the hit locations
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/centrifuge

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/centrifuge/centrifuge_salp.sh

          2. Remove contaminated contigs
               _Remove contigs with archae, bacterial or viral hits (ignore human hits) with number of reads >5 from min hit length 100 run_ <br>
                    *not many contaminated - 45 contigs*

               a. filter .out file to get just the contaminated contig IDs
                    #in this case there was only one tax id with >5 hits (32630) so first filtered to get just those rows (#of rows should = # in .tsv file; 45 in this case) | then get column #1 with the genome contig IDs | sort and uniq in case any of the hits are on the same contig (in this case they were not but in other cases they have been - especially if you have multiple taxons)
                         ```
                         awk -v OFS="\t" '$3 == '32630' {print}' centrifuge100min_Salp2019medaka_3169777.out | awk '{print $1}' | sort | uniq > centrifuge100min_Salp2019medaka_contamContigs.out
                         ```
                    
               b. remove contaminated contigs 
                         ```
                         source python_env/my_env/bin/activate
                         python filter_fasta_by_list_of_headers.py /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/purge_dups/consensus_3kblim/seqs/consensus_3kblim.purged.fa centrifuge100min_Salp2019medaka_contamContigs.out > SasperaAR06-13_shasta_medaka_3kblim_purgeDup_ContamRemoved.fasta
                         ```
               
          Check statistics using BUSCO and QUAST<br>
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/shasta_assembly/centrifuge/SasperaAR06-13_shasta_medaka_3kblim_purgeDup_ContamRemoved.fasta
                    
**2. assemble with Flye**
     A. assembly
          _input:_  
          /archive/projects/EBP/roneill/Salp/saspera_genomeassemb/FINALassembly_2021/Saspera_AR06-13_allCombined.fq

          _output:_
          /archive/projects/EBP/roneill/Salp/saspera_genomeassemb/FINALassembly_2021/

          _script:_
          /archive/projects/EBP/roneill/Salp/saspera_genomeassemb/FINALassembly_2021/flye_assembly/flye_assembly_saspera_ONTall.sh

               1. check quality with quast
               _input:_ flye assembly
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/Saspera_flye_June2021/assembly.fasta

               _output:_ sasperaAR0613_Flye
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/QUAST

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/QUAST/quast.sh

               2. check quality with busco
               _input:_flye genome
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/Saspera_flye_June2021/assembly.fasta

               _output:_ folder with stats: BUSCO_v5_Saspera_AR06-13_Flye
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/BUSCO

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/BUSCO/busco_v5.sh

     B. Impose a 3kb Limit on assembly
          1. remove line breaks
                awk '/^>/ { print (NR==1 ? "" : RS) $0; next } { printf "%s", $0 } END { printf RS }' assembly.fasta > assembly_noLineBreaks.fasta

          2.filter to remove contigs <3 kb
               _Input:_ /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/Saspera_flye_June2021/assembly_noLineBreaks.fasta 
               _Output:_ assembly_3kblim.fasta
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/3kblim
               _Script and location:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/3kblim/filtergenome_contigsize.sh
               Check statistics using BUSCO and QUAST<br>
          
          3. check quality with quast
               _input:_ flye assembly with 3 kb limit
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/3kblim/assembly_3kblim.fasta

               _output:_ sasperaAR0613_Flye3kblimit
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/QUAST

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/QUASTquast.sh

          4. check quality with busco
               _input:_flye genome with 3kb limit
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/3kblim/assembly_3kblim.fasta

               _output:_ folder with stats: BUSCO_v5_Saspera_AR06-13_Flye3kblim
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/BUSCO

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/BUSCO/busco_v5.sh


     C. Polish with medaka
          1. polish 
               _input:_ flye assembly + 3kb lim
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/3kblim/assembly_3kblim.fasta

               _output:_ consensus.fasta
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/medaka/medaka_flye_SasperaAR06-13/

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/medaka/medaka_polish.sh

          2. check quality with quast
               _input:_ flye genome with 3kb limit and medaka polished
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/medaka/medaka_flye_SasperaAR06-13/consensus.fasta

               _output:_ sasperaAR0613_Flye3kblim_medaka
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/QUAST

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/QUASTquast.sh

          3. check quality with busco
               _input:_flye genome with 3kb limit and medaka polished
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/medaka/medaka_flye_SasperaAR06-13/consensus.fasta

               _output:_ folder with stats: BUSCO_v5_Saspera_AR06-13_Flye3kblim_medaka
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/BUSCO

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/BUSCO/busco_v5.sh


     D. Purge dups
          1. make config file
               _input:_
                    1. pb.fofn which contains path to fq file: /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/Saspera_AR06-13_allCombined.fq
                    2. genome location: /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/medaka/medaka_flye_SasperaAR06-13/consensus.fasta

                    _output:_ config_Saspera_flye_medaka_3kblim.json
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups

                    _script:_
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/makeConfig.sh
               
               2. Run purge dups
                    _input:_ config_Saspera_flye_medaka_3kblim.json
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/

                    _output:_ final purged assembly: consensus.purged.fa 
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus_3kblim/seqs
                    *outputs four folders: coverage, purge_dups, seqs and split_aln*

                    _script:_
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/run_purge_dups.sh
               
               3. Check stats with quast
                    _input:_ flye assembled, medaka polishied, 3kb lim and purge dup assembly
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus/seqs/consensus.purged.fa

                    _output:_ sasperaAR0613_Flye3kblim_medaka_purgeDups folder
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/QUAST

                    _scripts:_
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/QUAST/quast.sh

               4. Check completeness with busco
                   _input:_ flye assembled, medaka polishied, 3kb lim and purge dup assembly
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus/seqs/consensus.purged.fa

                    _output:_ BUSCO_v5_Saspera_AR06-13_Flye3kblim_medaka_purgeDups folder
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/BUSCO/BUSCO/

                    _scripts:_
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/BUSCO/busco_v5.sh
               
               5. Try manual cutoffs with Purge Dup - went from 83.54% complete busco to 81.7% which is a bigger loss than I would like
                    a. make histogram
                         _input:_ PB.stat (automtaically output with purge dup)
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus/coverage/

                         _output:_  PB_histo.png
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus/coverage/

                         _script:_ run_histo_plot.sh
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus/coverage/
                    
                    b. Look at histogram and cutoffs file to see what cutoffs were used - seemed very tight around the first peak compared to what we do for purge haplotigs
                         #create new cutoffs file with manual cutoffs from looking at histogram - really only adjusted 2 of the values
                         cp cutoffs cutoffs_manual
                         cutoffs: 5       34      56      67      112     201
                         cutoffs_manual: 5       28      60      67      112     201
                    
                    c. Manually purge haplotigs and overlaps
                         _input:_
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus/coverage/cutoffs_manual
                         -c /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus/coverage/PB.base.cov
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus/split_aln/consensus.split.paf

                         _output:_ dups_manual.bed and purge_dups_manual.log
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus/manual_purgeDUP/

                         _script:_
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus/manual_purgeDUP/purge_dups_manual.sh
                    
                    d. Manually Get purged primary and haplotig sequences from draft assembly
                         _input:_ dups_manual.bed  and my draft genome
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus.fasta

                         _output:_ purged.fa (new purged genome) hap.fa (with haplotigs removed)
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus/manual_purgeDUP/

                         _script:_
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus/manual_purgeDUP/purge_dups_manual.sh

                         *check with busco and quast* not much different than the original run so did not use the manual cut offs

          E. Centrifuge - to remove contamination
               1. run centrifuge
                    _input:_ polished, haplotigs and <3kb contigs purged and the archael, viral, bacterial and human database (isg made)
                   /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus/seqs/consensus.purged.fa
                    /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v

                    _output:_ centrifuge_salp_flye2021medaka_report_minhit50.tsv, centrifuge_salp_flye2021medaka_report_minhit100.tsv with summary of hits and centrifuge100min_Salp2019medaka_3170495.out, centrifuge50min_Salp2019medaka_3170562.out
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge

                    _script:_
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/centrifuge_salp.sh

               2. Remove contaminated contigs
                    _Remove contigs with archae, bacterial or viral hits (ignore human hits) with number of reads >5 from min hit length 100 run_ <br>
                    *not many contaminated - 64 contigs*

                    a. filter .out file to get just the contaminated contig IDs
                         #in this case there was only one tax id with >5 hits (32630) so first filtered to get just those rows (#of rows should = # in .tsv file; 64 in this case) | then get column #1 with the genome contig IDs | sort and uniq in case any of the hits are on the same contig (in this case they were not but in other cases they have been - especially if you have multiple taxons)
                         ```
                         awk -v OFS="\t" '$3 == '32630' {print}' centrifuge100min_Salp2019medaka_3170495.out | awk '{print $1}' | sort | uniq > centrifuge100min_Salp2021_Flye_contamContigs.out
                         ```
                    
                    b. remove contaminated contigs 
                         ```
                         source python_env/my_env/bin/activate
                         python filter_fasta_by_list_of_headers.py /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/purge_dups/consensus/seqs/consensus.purged.fa centrifuge100min_Salp2021_Flye_contamContigs.out > SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta
                         ```
                    Check statistics using BUSCO and QUAST<br>
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta
               
               3. Test copepod datbase bc the salp rna maps at such a low % (although this DB was made specifically based on Sthomp stomach contents and known antarctic copepod species)
                        _Input:_ antarctic_copepod database and salp genome
                         /projects/EBP/Oneill/salp_genome_assembly/Salp451_2019Sep_genomeassemb/centrifuge/centrifuge_phytozoo/myDB/library/copepod
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta

                         _Output:_ cent_salp_copepods_minhit100.tsv
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge_copepodDB

                         _Script and location:_
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge_copepodDB/centrifuge_salp_copepods.sh

                         *only one hit (17 reads and 22) with 100 and 50 min hit*
                         *did not remove them*
               

                    
**final genome assembly is this flye assembly:
/projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta


**3. Assemble all reads (1D genomic ligation and WGA) runs with Shasta (A-E)**
A. combine all reads 
     /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/Saspera_ONTall_3.fq <- contains reads from A, C, D and E
     cp /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/Saspera_ONTall_3.fq /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/Saspera_ONTall_3_addJune2021.fq

     #add reads from B (2 promethion runs)
     cat /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_A_PAG25939/20210601_1834_3D_PAG25939_85be1ccc/fastq_pass/2021June01_AR06-13_A_PAG25939_combined.fq >> Saspera_ONTall_3_addJune2021.fq
     cat /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_B_PAG25945/20210601_1834_3F_PAG25945_b6df17ac/fastq_pass/2021June01_AR06-13_B_PAG25945_combined.fq >> Saspera_ONTall_3_addJune2021.fq

     made a script to do everything above and convert from fastq to fasta bc taking too long
          /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/copy_combine_reads.sh

*fail - runs out of memory*
*also runs out of memory with flye - only runs for ~24 hours*


**4. Make some plots of S.asper data and compare to S. thompsoni**
     A. Make a file with contig name and and size
          ```
          source home/CAM/kdivito/python_env/myenv_bin/activate
          python run_countSequenceLength.py
          ```

           _input:_
           /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta

          _output:_
          /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved_chrom.sizes

          _script:_
          /home/CAM/kdivito/scripts/countSequenceLength.py

               #edit to remove > and sort
                    sed -i 's/>//g' SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved_chrom.sizes
                    #sort by chrom size - largest to smallest
                    sort -n -r -k2 SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved_chrom.sizes > SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved_chromSorted.sizes
     
     B. Get flow cell QC data - just S. aspera
     https://github.com/roblanf/minion_qc

          1. Put all sequencing files in one place
               cp /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_A_PAG25939/20210601_1834_3D_PAG25939_85be1ccc/sequencing_summary_PAG25939_aafefd32.txt /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/sequencing_summaryFiles/June2021_runA/

               cp /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/2021JUN01_KC_AR06-13_B_PAG25945/20210601_1834_3F_PAG25945_b6df17ac/sequencing_summary_PAG25945_921b6157.txt /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/sequencing_summaryFiles/June2021_runB/

               cp /projects/EBP/Oneill/reads/nanopore/minion/salp_aspera_minion/2019OCT30_Sasp-AR06-18_MIN106_FAL53831_LSK109/2019OCT30_Sasp-AR06-18_MIN106_FAL53831_LSK109/20191030_1807_MN17898_FAL53831_99a6adf8/fastq_NEW/sequencing_summary.txt /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/sequencing_summaryFiles/Oct2018_minion/

               *put the files in separate folders bc all files must be named sequencing_summary.txt in order for the program to recognize them*

               _input:_ parent directory to sequencing summary files
               /projects/EBP/Oneill/reads/nanopore/promethion/salpa_aspera_promethion/sequencing_summaryFiles/

               _output:_ all_flowcells_combined_QC (stats and graphs)
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flowCell_QC

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flowCell_QC/run_minionQC.sh
               also need: MinIONQC.R

          2. Get flow cell QC data - S. aspera and S. thompsoni
               _input:_ parent directory to sequencing summary files - put all thomp and aspera ones together
               /projects/EBP/Oneill/salp_genome_assembly/flowcellQC_SthompSaspera_combined/salp_summary_files

               _output:_ all_flowcells_combined_QC (stats and graphs)
               /projects/EBP/Oneill/salp_genome_assembly/flowcellQC_SthompSaspera_combined

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/flowcellQC_SthompSaspera_combined/run_minionQC.sh
               also need: MinIONQC.R

**5. Repeat Annotate the flye assembly**
Create combine de novo repeat library

     A. Repeat Modeler
          1. Make database out of genome
                    _input:_ saspera assembly - all 1D genomic DNA by ligation - flye - medaka - 3kb lim - purge hap - contam remomved
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta

                    _output:_ database files with prefix SasperaAR0613
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatModeler

                    _script:_ 
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatModeler/RM_buildatabase.sh

          2. Run Repeat Modeler
                    _input:_ genome database made in step 1
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatModeler/SasperaAR0613

                    _output:_ SasperaAR0613-families.fa (Consensus sequences) and SasperaAR0613-families.stk (Seed alignments)
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatModeler/
                    _script:_ 
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatModeler/RepeatModeler.sh


     B. Transposon PSI
          *must also load legacy-blast/2.2.22 because it needs to use the formatdb utility*
          1. Run Transposon PSI
               _input:_ saspera assembly - all 1D genomic DNA by ligation - flye - medaka - 3kb lim - purge hap - contam remomved
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta

               _output:_ 
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/TransposonPSI/
               _script:_
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/TransposonPSI/transposonPSI.sh
          
          2. Pull out seqs into new fasta file to make Transposon PSI library
               *bedtools getfasta*
                    _input:_ saspera assembly - all 1D genomic DNA by ligation - flye - medaka - 3kb lim - purge hap - contam remomved
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/TransposonPSI/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta.TPSI.allHits.chains.bestPerLocus.gff3

                    _output:_ fasta file with repeats annotated by transposon PSI
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/TransposonPSI/Saspera_AR06-14_TransposonPSI_bestPerLocus.fasta

                    _script:_
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/TransposonPSI/bedtools_getfasta.sh

          3. Remove any sequences < 50 bp
                    _input:_ fasta file with repeats annotated by transposon PSI
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/TransposonPSI/Saspera_AR06-14_TransposonPSI_bestPerLocus.fasta

                    _output:_ filtered fasta file with repeats annotated by transposon PSI
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/TransposonPSI/Saspera_AR06-14_TransposonPSI_bestPerLocus_filter50bp.fasta

                    _script:_
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/TransposonPSI/filterFasta.sh

                    _Check how many sequences removed:_
                    ```
                    grep -e ">" Saspera_AR06-14_TransposonPSI_bestPerLocus.fasta | wc -l
                         #60610
                    grep -e ">" Saspera_AR06-14_TransposonPSI_bestPerLocus_filter50bp.fasta | wc -l
                         #60610
                    ```
                    *No sequences were less than 50. Double checked by runnnig Saspera_AR06-14_TransposonPSI_bestPerLocus.fasta with my run_countSequenceLength.py and the shortest sequence was 54 bp*
                    *delete Saspera_AR06-14_TransposonPSI_bestPerLocus_filter50bp.fasta*
          
          4. Remove the (-) and (+) in header because RepeatMasker does not like those symbols
               ```
               sed 's/(-)//g' Saspera_AR06-14_TransposonPSI_bestPerLocus.fasta | sed 's/(+)//g' > Saspera_AR06-14_TransposonPSI_bestPerLocus_headerEdit.fasta
               ```
  

     C. LTR harvest
          *used version genometools/1.5.10 but newer version available now*
          1. create the enhanced suffix array. 
               # We invoke gt suffixerator with options -tis, -suf,-lcp, -des, -ssp and -sds since LTRharvest needs the corresponding tables
               # specify -dna, as we process DNA-sequences.

               _input:_ saspera assembly - all 1D genomic DNA by ligation - flye - medaka - 3kb lim - purge hap - contam remomved
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta

               _output:_ index with prefix Saspera_AR0613_INDEX
                    .des, .esq, .lcp, .llv, .md5, .ois, .prj, .sds, .ssp, .suf
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/LTRharvest
               
                _script:_
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/LTRharvest/LTRharvest_index.sh

          2. Run LTR harvest
               _input:_ index from step a with prefix Saspera_AR0613_INDEX
                    /projects/EBP/Oneill/salp_genome_assembly/Salp451_2019Sep_genomeassemb/medaka/LTRharvest/

               _output:_ Saspera_AR0613_LTRharvest.fasta and Saspera_AR0613_LTRharvest.gff3
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/LTRharvest
                    
               _script:_
                   /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/LTRharvestLTRharvest.sh
          
          3. Run LTR digest to remove any false positive hits and any LTRs without domain hits
                    *Previously downloaded Pfam and GyDB HMM files*
                    1.sort gff
                         module load genometools/1.5.10
                         ```
                         gt gff3 -sort Saspera_AR0613_LTRharvest.gff3 > Saspera_AR0613_LTRharvestSORTED.gff3
                         ```
                    2. run ltr digest
                    _input:_ sorted gff3, hmms file location, index prefix (Saspera_AR0613_INDEX)
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/LTRharvest/Saspera_AR0613_LTRharvestSORTED.gff3
                    /projects/EBP/Oneill/salp_genome_assembly/pfam_GyDB_HMM/*.hmm 
                    _output:_ new gff3 and all the fasta files for each group, complete fasta file with all annotated LTRs, csv file (accidentally forgot to change the output filename)
                    Saspera_AR0613_LTRharvestDigest.gff3
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/LTRharvest
                   
                    _script:_
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/LTRharvest/LTRdigest.sh

     D. Combine Libraries
          1.combine
          _location:_ /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations
               ```
               cp /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/LTRharvest/Saspera_AR0613_LTRharvestDigest_complete.fas Saspera_combinedRepeatLib.fa
               cat /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/TransposonPSI/Saspera_AR06-14_TransposonPSI_bestPerLocus_headerEdit.fasta >> Saspera_combinedRepeatLib.fa
               cat /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatModeler/SasperaAR0613-families.fa  >> Saspera_combinedRepeatLib.fa
               ```

          2. remove redundant sequences
               _input:_ combine repeat library
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/Saspera_combinedRepeatLib.fa

               _output:_ Saspera_combinedRepeatLib_cluster0.8.fa
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/combined_clusteredLibrary

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/combined_clusteredLibrary/usearch.sh

               grep ">" Saspera_combinedRepeatLib.fa | wc -l
                    #88848 - original number of sequences
               grep ">" Saspera_combinedRepeatLib_cluster0.8.fa | wc -l
                    #35765 - after clustering - 40 % of sequences clustered

          3. Repeat Classify
               _input:_ combined repeat library with redundant sequences removed
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/combined_clusteredLibrary/Saspera_combinedRepeatLib_cluster0.8.fa
               
               _output:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/combineLib_Classified

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/combineLib_Classified/Repeatclassifier.sh

***old repeat notes***
                    #grep headers from repeat modeler
                         ```
                         grep -e "rnd" sthompsoni2019Medaka-combinedRepeatLib_headerEDIT_COPY.fa.classified > headers.txt
                         ```
                         put in excel and remove redundant to make a list
                         56 classifications
                    #fix with sed (-i fix in file) used | instead of / otherwise it wont fix some of the classifications that are separated by /
                    ```
                   remove line breaks:
        awk '/^>/ { print (NR==1 ? "" : RS) $0; next } { printf "%s", $0 } END { printf RS }' Sthompsoni_RepeatLibCombined.fasta > Sthompsoni_RepeatLibCombined.fasta


     B. Repeatmask genome using RepeatMasker
          1. Use repeat modeler library only
               _input:_ RepeatModeler library and final genome
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatModeler/SasperaAR0613-families.fa
               SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta (*I copy the genome into the directory and once the run is over I delete it to make space*)

               _output:_ SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta.masked, .out, .out.gff, .tbl
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatMasker_RMonly

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatMasker_RMonly/RepeatMasker_RMlib.sh


          2. Combined repeat library
               _input:_ combined repeat library from step A and final genome
/              projects/EBP/Oneill/salp_genome_assembly/Salp451_2019Sep_genomeassemb/medaka/RepeatLib_combined/sthompsoni2019Medaka-combinedRepeatLib_headerEDIT.fa.classified 
               *use rmblast search engine instead of cross_match because cross_match kept erroring*

               _output:_ SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta.masked, .out, .out.gff, .tbl
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatMasker_combinedLib_rmblast

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatMasker_combinedLib_rmblast/RepeatMasker_denovolib.sh


               #Notes from from when cross_match wasf ailing:
                    #error: WARNING: Comparison failed. Retrying with larger minmatch (10) and batch failure
                    #checked library headers and reduced them to be < 50 bp - still failed
                    #tried to split up genome which also failed
                    # tried addign the -frag 1000 flag which also failed
                    #check batch that is failing (even though it is a different batch everytime)
                         /isg/shared/apps/RepeatMasker/4.1.1/util/getRepeatMaskerBatch.pl -batch 366 SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta
                         Creating Batch 366 seq count = 1 len = 58580, average GC = 36
                         Creating Batch 25 seq count = 1 len = 53194, average GC = 40
                         -seems like a lot of the sequences are really long so check all the sequence lengths and remove ones over a specific length?
                         - nothing about the sequence looks odd
                    #submitted a help ticket to the repeatmasker git - suggested getting more detailed log by doing this (the command is copied from the .out file):
                    To get some additional information about the error that happened, could you re-run one of those commands listed for the "engine parameters" in a failed run - and keep the full output in a file? 
                    The cm_output.txt file should have a more detailed error, that may solve the problem or at least point in the right direction.
                    ```
                    /isg/shared/apps/phrap/1.090518/cross_match  -alignments -gap_init -30 -ins_gap_ext -6 -del_gap_ext -5 -minmatch 10 -minscore 225 -bandwidth 14 -masklevel 101 -matrix /isg/shared/apps/RepeatMasker/4.0.9-p2/Matrices/crossmatch/20p39g.matrix /dev/shm/kdivito/RM_557161.TueAug100917122021/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved_split00.fa_batch-25.masked /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/combined_clusteredLibrary//Saspera_combinedRepeatLib_cluster0.8edit.fa.classified
                    ```
                    #from that suggested it was a search engine issue so I switched to rmblast which worked on one fo the split files and finished after 5 days on the full genomes

          3. Convert repeat gff to bed file
               a. Convert
               _input:_ SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta.out
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatMasker_combinedLib_rmblast

               _output:_  SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved_rm.bed
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatMasker_combinedLib_rmblast

               _python script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatMasker_combinedLib_rmblast/RM2Bed.py
               https://github.com/rmhubley/RepeatMasker/blob/master/util/RM2Bed.py

               _script to run:_ 
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatMasker_combinedLib_rmblast/RM2bed.sh

               #sort
               sort SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved_rm.bed > SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved_rm_sort.bed

               #headers:
               chr start end rep_type size direction rep_class rep_family
          
          4. Look at overlaps with ULTRA
               _input:_ ULTRA 2000 period saspera file and the repeat annotations from the combined repeat library (bed format)
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/ULTRA2000_Saspera_genome.bed
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/RepeatMasker_combinedLib_rmblast/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved_rm_sort.bed \

               _output:_ two output files:
               1) ULTRA repeats that do not overlap with the repeat masker library: Saspera_2000ULTRA_noRM.bed
               2) repeats that overlap in both the ULTRA and repeatmasker library: Saspera_2000ULTRA_RMoverlaps.bed
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/ULTRA

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/RepeatAnnotations/ULTRAbedtools_intersect_RM_ULTRA.sh

               wc -l Saspera_2000ULTRA_noRM.bed
               # 104831 with no overlap

               wc -l /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/ULTRA/2000_Saspera_genome.bed
               # 1111484 total ULTRA repeats
               # 104831/1111484 = 9.4% were novel

**6. Map Omni-C data and see if I can scaffold**
     srun --partition=general --qos=general --mem=1G --pty bash
     #activate dovetail environment: *must be in this environment for scripts to work*
          conda activate dovetail
          (/projects/EBP/Oneill/salp_genome_assembly/OmniC)

     A. Make genome into BWA index
         *must be in the dovetail conda env*
           _location:_ /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/
               
               ```
               bwa index SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta
               ```
     
     B. Map test data (shallow sequencing run 1-2M reads) to new genome
          _input:_ saspera assembly - all 1D genomic DNA by ligation - flye - medaka - 3kb lim - purge hap - contam remomved
          /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta

          _output:_ files with prefix: AR06-14_omnic_finaSasperaGenome_chk
          summary stats: omnic_qc_3173799.out
          /projects/EBP/Oneill/salp_genome_assembly/OmniC/Saspera_AR06-14_map2finalSaspGenome

          _script:_
          /projects/EBP/Oneill/salp_genome_assembly/OmniC/omnic_qc.sh

          ```
          #summary stats
          Total Read Pairs                              770,178     100%
          Unmapped Read Pairs                           365,768     47.49%
          Mapped Read Pairs                             125,941     16.35%
          PCR Dup Read Pairs                            475         0.06%
          No-Dup Read Pairs                             125,466     16.29%
          No-Dup Cis Read Pairs                         36,362      28.98%
          No-Dup Trans Read Pairs                       89,104      71.02%
          No-Dup Valid Read Pairs (cis >= 1kb + trans)  106,703     85.05%
          No-Dup Cis Read Pairs < 1kb                   18,763      14.95%
          No-Dup Cis Read Pairs >= 1kb                  17,599      14.03%
          No-Dup Cis Read Pairs >= 10kb                 10,032      8.0%
          Expected unique pairs at 300M sequencing      45,791,903  NA
          ```
     
     C. Map full run to this genome and see if I can scaffold    
          #start interactive session and conda env (created above)
          cd /projects/EBP/Oneill/salp_genome_assembly/OmniC
          conda activate dovetail
               *must be in interactive session when submit scripts*

          1. pre-alignment (https://omni-c.readthedocs.io/en/latest/pre_alignment.html)
               a. already have file with chrom name and size (two columns)
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved_chrom.sizes
               b already made bwa index for this genome 
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta
               c. temp directory alread made
                    /home/CAM/kdivito/temp

          2. From fastq to final valid pairs bam file (https://omni-c.readthedocs.io/en/latest/fastq_to_bam.html)
          *can do all in one command but did each individually*
               a. map with bwa
                    necessary parameters:
                         mem: set the bwa to use the BWA-MEM algorithm, a fast and accurate alignment algorithm optimized for sequences in the range of 70bp to 1Mbp
                         -5: for split alignment, take the alignment with the smallest coordinate (5’ end) as primary, the mapq assignment of the primary alignment is calculated independent of the 3’ alignment
                         -S: skip mate rescue
                         -P: skip pairing; mate rescue performed unless -S also in use
                         -T0: The T flag set the minimum mapping quality of alignments to output, at this stage we want all the alignments to be recorded and thus T is set up to 0, (this will allow us to gather full stats of the library, at later stage we will filter the alignments by mapping quality
                         -t: number of threads, default is 1. Set the numbers of threads to not more than the number of cores that you have on your machine (If you don’d know the number of cores, used the command lscpu and multiply Thread(s) per core x Core(s) per socket x Socket(s))
                         *.fasta or *.fa : Path to a reference file, ending with .fa or .fasta, e,g, hg38.fasta
                         *.fastq or *.fastq.gz: Path to two fastq files; path to read 1 fastq file, followed by fastq file of read 2 (usually labeled as R1 and R2, respectively). Files can be in their compressed format (.fastq.gz) or uncompressed (.fastq). In case your library sequence is divided to multiple fastq files, you can use a process substitution < with the cat command (see example below)
                         -o: sam file name to use for output results [stdout]. You can choose to skip the -o flag if you are piping the output to the next command using ‘|
                         
                    _input:_
                    /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta
                    /projects/EBP/Oneill/reads/illumina/salp/OmniC/Kate_OmniC_June2021/AR06-14_S2_R1_combined.fastq.gz
                    /projects/EBP/Oneill/reads/illumina/salp/OmniC/Kate_OmniC_June2021/AR06-14_S2_R2_combined.fastq.gz

                    _output:_ Saspera_AR06-14_aligned.sam
                    /home/CAM/kdivito/Kate_OmniC_AR06-14_Saspera

                    _Script:_ /home/CAM/kdivito/Kate_OmniC_AR06-14_Saspera/01_bwamem.sh
                    *can submit and will run as long as in dovetail env when submit script*
                    *do not load bwa from xanadu - will use bwa in dovetail env*
          
               b. Record valid ligations
                    We use the parse module of the pairtools pipeline to find ligation junctions in Omni-C (and other proximity ligation) libraries. When a ligation event is identified in the alignment file the pairtools pipeline will record the outer-most (5’) aligned base pair and the strand of each one of the paired reads into .pairsam file (pairsam format captures SAM entries together with the Hi-C pair information). In addition, it will also asign a pair type for each event. e.g. if both reads aligned uniquely to only one region in the genome, the type UU (Unique-Unique) will be assigned to the pair. The following steps are necessary to identify the high quality valid pairs over low quality events (e.g. due to low mapping quality):

                         min-mapq=40: Mapq threshold for defining an alignment as a multi-mapping alignment. Alignment with mapq <40 will be marked as type M (multi)
                         walks-policy=5unique:Walks is the term used to describe multiple ligations events, resulting three alignments (instead of two) for a read pair. However, there are cases in which three alignment in read pairs are the result of one ligation event, pairtool parse can rescue this event. walks-policy is the policy for reporting un-rescuable walk. 5unique is used to report the 5’-most unique alignment on each side, if present (one or both sides may map to different locations on the genome, producing more than two alignments per DNA molecule)
                         max-inter-align-gap=30: In cases where there is a gap between alignments, if the gap is 30 or smaller, ignore the gap, if the gap is >30bp, mark as “null” alignment
                         nproc-in=integer, e.g. 16: pairtools has an automatic-guess function to identify the format of the input file, whether it is compressed or not. When needed, the input is decompressed by bgzip/lz4c. The option nproc-in set the number of processes used by the auto-guessed input decompressing command, if not specified, default is 3
                         nproc-out=integer, e.g. 16: pairtools automatic-guess the desired format of the output file (compressed or not compressed, based on file name extension). When needed, the output is compressed by bgzip/lz4c. The option nproc-out set the number of processes used by the auto-guessed output compressing command, if not specified, default is 8
                         chroms-path: path to .genome file, e.g. hg38.genome
                         *.sam: path to sam file used as an input. If you are piping the input (stdin) skip this option
                         *pairsam: name of pairsam file for writing output results. You can choose to skip and pipe the output directly to the next commad (pairtools sort)
                         
                    #identify high quality read pairs/record valid ligation events
                    pairtools parse --min-mapq 40 --walks-policy 5unique --max-inter-align-gap 30 --nproc-in 8 --nproc-out 8 \
                    --chroms-path /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta \
                    Saspera_AR06-14_aligned.sam > Saspera_AR06-14_omnic_parsed.pairsam

                    #sort pairsam file
                    pairtools sort --nproc 16 --tmpdir=/home/CAM/kdivito/temp/ Saspera_AR06-14_omnic_parsed.pairsam > Saspera_AR06-14_omnic_parsed_sorted.pairsam

                    #remove PCR duplicates
                    –mark-dups: If specified, duplicate pairs are marked as DD in “pair_type” and as a duplicate in the sam entries
                    –output-stats: Output file for duplicate statistics. Please note that if a file with the same name already exists, it will be opened in the append mode

                    pairtools dedup --nproc-in 8 --nproc-out 8 --mark-dups --output-stats stats.txt \
                    --output Saspera_AR06-14_omnic_parsed_dedup.pairsam Saspera_AR06-14_omnic_parsed_sort.pairsam

                         #to get a summary table of stats:
                         python get_qc.py -p stats.txt > stats_summary.txt
                              Total Read Pairs                              385,502,767  100%
                              Unmapped Read Pairs                           143,320,888  37.18%
                              Mapped Read Pairs                             96,774,286   25.1%
                              PCR Dup Read Pairs                            89,445,881   23.2%
                              No-Dup Read Pairs                             7,328,405    1.9%
                              No-Dup Cis Read Pairs                         3,716,221    50.71%
                              No-Dup Trans Read Pairs                       3,612,184    49.29%
                              No-Dup Valid Read Pairs (cis >= 1kb + trans)  4,269,833    58.26%
                              No-Dup Cis Read Pairs < 1kb                   3,058,572    41.74%
                              No-Dup Cis Read Pairs >= 1kb                  657,649      8.97%
                              No-Dup Cis Read Pairs >= 10kb                 366,094      5.0%
                              put into a file called stats_summary.txt

                    #generate .pairs and bam file (split files into pair and bam files)
                    pairtools split --nproc-in 8 --nproc-out 8 --output-pairs Saspera_AR06-14_mapped.pairs \
                    --output-sam Saspera_AR06-14_omnic_parsed_dedup_unsorted.bam Saspera_AR06-14_omnic_parsed_dedup.pairsam
                         *The .pairs file can be used for generating contact matrix*

                    #generate final bam file (sort and index)
                    samtools sort -@16 -T /home/CAM/kdivito/temp/temp.bam -o Saspera_AR06-14_mapped.PT.bam Saspera_AR06-14_omnic_parsed_dedup_unsorted.bam
                    #index bam file
                    samtools index Saspera_AR06-14_mapped.PT.bam
                         #final bam used in downstream steps

                    _script:_ all parsing steps in one script: /home/CAM/kdivito/Kate_OmniC_AR06-14_Saspera/02_validLigations.sh
               
               d. Scaffold with JUICER/3D-DNA
                    1. create genome index with bwa and map omni-c reads to the genome
                         *already have genome as bwa index*
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta
                         omni-c files: /home/CAM/kdivito/Kate_OmniC_AR06-14_Saspera/JUICER/fastq/AR06-14_S2_R1_combined.fastq.gz
                         /home/CAM/kdivito/Kate_OmniC_AR06-14_Saspera/JUICER/fastq/AR06-14_S2_R2_combined.fastq.gz
                         _output:_
                              /home/CAM/kdivito/Kate_OmniC_Sato1236/JUICER
                         _script:_
                              /home/CAM/kdivito/Kate_OmniC_AR06-14_Saspera/JUICER/01_bwaindex_mem.sh




          2. Run Juicer to align, need merged_nodups.txt for 3D-DNA
               a. requires certain folder organization -> Juicer expects the fastq files to be stored in a directory underneath the top-level directory (top level directory is where    you run your juicer script)
                    omic fastq files copied to here: /home/CAM/kd/home/CAM/kdivito/Kate_OmniC_AR06-14_Saspera/JUICER/fastq
                    run script from here: /home/CAM/kdivito/Kate_OmniC_AR06-14_Saspera/JUICER

               b. make chromosome size file
                    already have
                     /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved_chrom.sizes

               c. run juicer
                    _input:_
                    script: /home/CAM/kdivito/Kate_OmniC_Sato1236/JUICER/omnic_fastq/scripts/juicer.sh \
                    genome (with bwa index int he same folder - step a): /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta
                    chrom size file (Stepb b): /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved_chrom.sizes

                    _output:_
                    
                    _Script:_​
                    /home/CAM/kdivito/Kate_OmniC_Sato1236/JUICER/02_juicer.sh

               ***Running this script "completes" but does not go through all of the split aligned files. 
                    Deleted debug and aligned folders and all files except for split.fastq files in splits directory. 
                    Reran above juicer script again, which now skips the splitting files section. Looks like its running properly on all files now.
               ​

               3. Run 3D-DNA to scaffold
               _location:_ /home/CAM/kdivito/Kate_OmniC_Sato1236/3D-DNA

               #copy the 3D-DNA scripts from gabby
               cp -r /home/FCAM/ghartley/3D-DNA/3d-dna-master/ .

               #make tmp directory
               mkdir /labs/Oneill/kcastellano/hic_tmp/

                    _input:_ genome and merged_nodups.txt file from juicer
                    /projects/EBP/Oneill/salp_genome_assembly/Salp451_2019Sep_genomeassemb/medaka/purgeHaplotigs/curated.fasta 
                    /home/CAM/kdivito/Kate_OmniC_Sato1236/JUICER/aligned/merged_nodups.txt

                    _script:_
                    /home/FCAM/ghartley/3D-DNA/3d-dna-master/ 

**7. Map S. thompsoni RNA reads to S. aspera genome**
     #check the quality of the assembly but also how well the thompsoni reads map will tell me if I can use them for gene annotations

     A. Make genome into hisat 2 index
          _input:_ saspera assembly - all 1D genomic DNA by ligation - flye - medaka - 3kb lim - purge hap - contam remomved
          /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta

          _output:_ Saspera_AR06-14_hisat2Index
          /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/hisat2

          _Script:_
          /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/hisat2/hisat2_mkindex.sh
     
     B. Map each sample and all combined reads (raw reads not transcriptome)
          _input:_
          genome index from A: /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/Saspera_AR06-14_hisat2Index
          individual raw reads per sample located here: /labs/Oneill/kcastellano/salp_transcriptome/2019Aug_total_mRNA/trimmed

          _output:_ <samplename>_SasperaGenome_hisat2.sam
          /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/hisat2

          _Script:_
          /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/hisat2/hisat2_genomemap.sh
     
     C. Convert sam to bam files
          _input:_

          _output:_

          _script:_
          /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/hisat2/samtools_cvrtsortindex.sh

**8. Map ONT DNA reads back to assembly to check for major assembly problems**fasta
     _input:_saspera assembly - all 1D genomic DNA by ligation - flye - medaka - 3kb lim - purge hap - contam remomved
          /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.

          combined reads:/archive/projects/EBP/roneill/Salp/saspera_genomeassemb/FINALassembly_2021/Saspera_AR06-13_allCombined.fq

     _output:_
     /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/minimap2

     _script:_
     /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/minimap2

**9. Look for G4s**
     **all scripts can be found in the S. thompsoni git repository**
     A. run G4hunter
          _input:_
          ra assembly - all 1D genomic DNA by ligation - flye - medaka - 3kb lim - purge hap - contam remomved
          /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved.fasta

          _output:_ folder: Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved
          /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved

          _script:_
          /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved/G4hunter.sh
     
     B # of Contigs
          ```
          grep ">" SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged.txt | wc -l
          #7554
          ```
     
     C. count the number of potentially G-quadruplex sequences on each contig to get a genome view
          script: /home/CAM/kdivito/scripts/G4_pythonscripts/countG4Function.py
          run with: /home/CAM/kdivito/scripts/G4_pythonscripts/run_countG4Function.py

          input: SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged.txt
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved

          output: SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged_G4contigcount.txt
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved

          sed -i 's/>//g' SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged_G4contigcount.txt
     
     D. Calculate general summary stats
          script: /home/CAM/kdivito/scripts/G4_pythonscripts/minMaxMedianMeanSum_G4MergedFile.py
          to run: /home/CAM/kdivito/scripts/G4_pythonscripts/run_minMaxMedianMeanSum_G4MergedFile.py
          
          input: SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged_G4contigcount.txt
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved

          output: SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged_G4contigcount_summary.csv
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved


     E. Convert to a bed file
          _input:_ SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged.txt
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved

          _output:_ SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged.bed
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved

          _script:_
          /projects/EBP/Oneill/salp_genome_assembly/Salp451_2019Sep_genomeassemb/medaka/G4hunter/Results_curated/G4_to_bed.py

               to run:
               ```
               source /home/CAM/kdivito/python_env/my_env/bin/activate
               python G4_to_bed.py
               ```
               #for some reason this script uses dos window coding so instead of a new line it has a "^M" character - fix it like this:
               ```
               sed -i -e "s/\r/\n/g" SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged.bed
               ```
               #remove > infront of contig # using sed*
               ```
               sed -i 's/>//' SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged.bed
               ```

          #add G4 ID and sequence to bed file
               1.add G4_ID
                    awk -v OFS="\t" '{$(NF+1) = "G4_"(NR-1)} 1' SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged.bed > SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged_addG4IDs.bed
               
               2. add sequence
                    a. get sequences on their own
                         awk '{print $3}' SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged.txt | sed '/Sequence/d' | sed '/^$/d' > G4_seqsOnly.txt
                    
                    b. merge with the bed file (the way I made both files means I can just paste them together without doing any sorting or matching)
                         paste -d"\t" SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged_addG4IDs.bed G4_seqsOnly.txt > SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged_addG4info2.bed

          # creat bed file with G4 directionality
               1. remove the contig headers | remove headers (lines containing Start)
                         sed -e '/>/'d SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged.txt | sed -e '/Start/'d > SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged_noHeaders.txt
               2. now you can just paste the bed file and edited merged.txt file because we have not changed the order of lines
                         paste SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged_addG4IDs.bed SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged_noHeaders.txt > intermediate.txt
                    #remove unwanted columns
                         awk -v OFS='\t' '{print $1,$2,$3,$4,$9} intermediate.txt > Saspera_G4motifs_plusDirection.bed
                    #the direction is given with the score; - = on the - strand (or C stretch), positive number = on the + strand (or G stretchs)
                         #remove numbers after - sign
                         sed -i 's/-.*/-/g' Saspera_G4motifs_plusDirection.bed
                         #rem positive numbers and replace them with a + sign (scores are from 0-4)
                         sed -i 's/1\..*/+/g' Saspera_G4motifs_plusDirection.bed
                         sed -i 's/0\..*/+/g' Saspera_G4motifs_plusDirection.bed
                         sed -i 's/2\..*/+/g' Saspera_G4motifs_plusDirection.bed
                         sed -i 's/3\..*/+/g' Saspera_G4motifs_plusDirection.bed
                         sed -i 's/4\..*/+/g' Saspera_G4motifs_plusDirection.bed
                              #to check that you have converted all numbers do this: awk '{print $5}' Saspera_G4motifs_plusDirection.bed | sort | uniq 
                                   #should only have + and - like this:
                                        -
                                        +
                    #remove intermediate file

     F. G4 genome wide distribution
          1. Calculate how often we would expect them if equally distributed
               total sequence length/total # of G4s = bp length to expect a G4 (see excel sheet)

          3. Merge overlapping G4's to make sure that you dont count nucleotides multiple times
               _input:_ SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved-Merged.txt (G4hunter output file)
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved/

               _output:_ Saspera_G4ID_score_plusDirection_bedtoolsMerge.bed and Saspera_G4ID_score_plusDirection_bedtoolsMerge_seqs.fasta
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved/bedtoolsMerge/

               _script:_
               /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved/bedtoolsMerge/mergeG4file_getSeqs.sh


          2. Calculate G4 abundance per 10 kb
               a. make the genome into 10kb windows
                    _location:_ /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge

                         input is a file with two columns: chromosome name and length
                         ```
                         module load BEDtools/2.29.0
                         bedtools makewindows -g SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved_chromSorted.sizes -w 10000 | sort -k1,1 -k2,2n > SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved_window-10kb.bed
                         ```
                         #total number of 10kb fragments
                              wc -l SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved_window-10kb.bed
                              #94199

               b. Count number of predicted G4 seqs per 10kb window
                    _location:_ /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/G4_distribution
                         ```
                         module load BEDtools/2.29.0
                         bedtools map -c 2 -o count -a /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/centrifuge/SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved_window-10kb.bed \
                         -b /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved/bedtoolsMerge/Saspera_G4ID_score_plusDirection_bedtoolsMerge.bed > saspera_G4abundance_10kbwindows_count.bed
                         ```
                         #get max count
                         sort -k 4 -n -r saspera_G4abundance_10kbwindows_count.bed | head -n 5
                         #get min count
                         sort -k 4 -n saspera_G4abundance_10kbwindows_count.bed | head -n 5

                         #how mnay regions have no G4's
                         awk '{print $4}' saspera_G4abundance_10kbwindows_count.bed | grep "0" | wc -l
                              #12357
                              #total number of regions
                              wc -l G4abundance_10kbwindows_count.bed
                                   #94199

          c. Get G4 clusters (>4 G4 motifs)
               Look for clusters of G4s
                    a. make a bed file including id, score and direction
                         #need to add a column 5 with "score" used 0 (needs to be between 0-1000)
                         cp Saspera_G4motifs_plusDirection.bed Saspera_G4motifs_plusDirection_toedit.bed
                         #get the first four columns (chr, start, end, id) and add a column with score (0 in this case just as a place holder)
                         awk -v OFS="\t" '{print $1,$2,$3,$4}' Saspera_G4motifs_plusDirection_toedit.bed | sed 's/$/\t0/g' > Saspera_G4motifs_plusDirection_toedit2.bed
                         #paste the two files together to get the direction
                         paste Saspera_G4motifs_plusDirection_toedit2.bed Saspera_G4motifs_plusDirection.bed > intermediate.bed
                         #get rid of columns dont need
                         awk -v OFS="\t" '{print $1,$2,$3,$4,$5,$10}' intermediate.bed >Saspera_G4motifs_id_score_direction.bed
                         *rm all the intermediate files*

                    b bedtools clusters
                         *-s flag to require strandedness

                         _input:_ /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved/Saspera_G4motifs_id_score_direction.bed 

                         _output:_ Saspera_G4_clusters.bed - adds a fourth column with cluster ID - so regions that cluster will all have the same ID
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved/G4clusters

                         _script:_
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved/G4clusters/bedtools_cluster.sh

                    c. created python script to get regions with >= 4 G4 "motifs"
                         This python script has notes with each command but what it does is counts the occurance of the bedtools cluster id (in column 6) and filters regions with >=4 motifs and prints them in a file
                         _input:_ Saspera_G4_clusters.bed
                         /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved/G4clusters

                         _output:_ Saspera_G4Clusters_4greater.csv and .tsv files (columns = Chr, start, end, G4 id, score (arbitrary in this case so will always be 0), strandedness, bedtools clusterid, occurance of that id [from python script])
                         /projects/EBP/Oneill/salp_genome_assembly/Salp451_2019Sep_genomeassemb/medaka/G4hunter/clustersOfG4

                         _script:_
                         /home/FCAM/kdivito/scripts/G4_pythonscripts/G4_filter_clusters.py
                         #edit input and output file names
                         #to run: python G4_filter_clusters.py
                         *also copied to /home/CAM/kdivito/scripts/G4_pythonscripts*

                         #make file with one unique line per "cluster" with bedtools merge
                                   #sort file
                                   bedtools sort -i Saspera_G4Clusters_4greater.tsv > Saspera_G4Clusters_4greater_sort.tsv
                                   # -c and -o together will keep column 6 (with directionality info) and print it to the file
                                   bedtools merge -s -c 6 -o distinct -i Saspera_G4Clusters_4greater_sort.tsv > Saspera_G4Clusters_4greater_sort_merge.tsv

                         #How many contigs have regions with >=4 motifs
                              awk '{print $1}' Saspera_G4Clusters_4greater_sort_merge.tsv | sort | uniq | wc -l
                                  #4061

                         #How many of the clusters per contig? Are there any contigs with a lot?
                              -Use python/pandas groupby
                              _input:_ Saspera_G4Clusters_4greater_sort_merge.tsv
                              /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved/G4clusters

                              _output:_ G4_clusterAbundance_perContig.tsv
                              /projects/EBP/Oneill/salp_genome_assembly/saspera_genomeassemb/FINALassembly_2021/flye_assembly/G4hunter/Results_SasperaAR06-13_flye_medaka_3kblim_purgeDup_ContamRemoved/G4clusters

                              _script:_
                              /home/FCAM/kdivito/scripts/G4_pythonscripts/G4cluster_abundancePerContig.py

                              #the most clusters on a contig = 160
                              sort -k2 -n -r Saspera_G4_clusterAbundance_perContig.tsv | head -n5